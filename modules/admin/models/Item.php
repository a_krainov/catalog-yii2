<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $category_id
 * @property string $vendor
 * @property string $type
 * @property string $opening
 * @property string $geometry
 * @property string $type_windows
 * @property string $color_outside
 * @property string $color_inside
 * @property int $height
 * @property int $width
 * @property string $options
 *
 * @property Tree $category
 * @property ItemFile[] $itemFiles
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['category_id', 'height', 'width'], 'integer'],
            [['title', 'vendor', 'type', 'opening', 'geometry', 'type_windows', 'color_outside', 'color_inside', 'options'], 'string', 'max' => 255],
            /*[['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tree::className(), 'targetAttribute' => ['category_id' => 'id']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'category_id' => 'Category ID',
            'vendor' => 'Vendor',
            'type' => 'Type',
            'opening' => 'Opening',
            'geometry' => 'Geometry',
            'type_windows' => 'Type Windows',
            'color_outside' => 'Color Outside',
            'color_inside' => 'Color Inside',
            'height' => 'Height',
            'width' => 'Width',
            'options' => 'Options',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Tree::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemFiles()
    {
        return $this->hasMany(ItemFile::className(), ['item_id' => 'id']);
    }
}
