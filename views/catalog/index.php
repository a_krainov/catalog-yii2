<?php

/* @var $this yii\web\View */

$this->title = 'Каталог окон';

?>
<div class="container mb60 page-styling row-wrap-container row-wrap-nottl">
    <p class="maincont-subttl">Популярные товары</p>
    <h2 id="catalog-main" class="mb35 heading-multishop">Каталог окон</h2>
    <div class="row prod-items prod-items-3">
        <? foreach ($items as $item) {
        ?>
        <article class="cf-sm-6 cf-md-4 cf-lg-4 col-xs-6 col-sm-6 col-md-4 col-lg-4 sectgl-item">
            <div class="sectgl prod-i">
                <div class="prod-i-top">
                    <a class="prod-i-img" href="<?=\yii\helpers\Url::to(['catalog/view', 'id'=> $item->id])?>">
                        <img src="http://placehold.it/290x258" alt="">
                    </a>
                    <div class="prod-i-actions">
                        <div class="prod-i-actions-in">
                            <p class="prod-quickview">
                                <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Быстрый просмотр</span></a>
                            </p>
                            <p class="prod-i-cart">
                                <a href="#" class="hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Добавить в корзину</span></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="prod-i-bot">
                    <div class="prod-i-info">
                        <p class="prod-i-price">$120.00</p>
                        <p class="prod-i-categ"><a href="<?=\yii\helpers\Url::to(['catalog/view', 'id'=> $item->id])?>"><?=$item->type?></a></p>
                    </div>
                    <h3 class="prod-i-ttl"><a href="<?=\yii\helpers\Url::to(['catalog/view', 'id'=> $item->id])?>"><?=$item->title?></a></h3>
                </div>
            </div>
        </article>
        <?}?>
    </div>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages])?>
    <p class="special-more">
        <a class="special-more-btn" href="#">Показать ещё</a>
    </p>

</div>

