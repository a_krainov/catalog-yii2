<?php

/* @var $this yii\web\View */

$this->title = $item->title;

$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<article>
    <div class="prod">
        <div class="prod-slider-wrap prod-slider-shown">
            <div class="flexslider prod-slider" id="prod-slider">
                <ul class="slides">
                    <li>
                        <a data-fancybox-group="prod" class="fancy-img" href="http://placehold.it/810x722">
                            <img src="http://placehold.it/550x490" alt="">
                        </a>
                    </li>
                    <li>
                        <a data-fancybox-group="prod" class="fancy-img" href="http://placehold.it/800x800">
                            <img src="http://placehold.it/550x550" alt="">
                        </a>
                    </li>
                </ul>
                <div class="prod-slider-count"><p><span class="count-cur">1</span> / <span class="count-all">2</span></p><p class="hover-label prod-slider-zoom"><i class="icon ion-search"></i><span>Zoom In</span></p></div>
            </div>
            <div class="flexslider prod-thumbs" id="prod-thumbs">
                <ul class="slides">
                    <li>
                        <img src="http://placehold.it/550x490" alt="">
                    </li>
                    <li>
                        <img src="http://placehold.it/550x550" alt="">
                    </li>
                </ul>
            </div>
        </div>

        <div class="prod-cont">
            <p class="prod-categs"><a href="#"><?=$item->type?></a></p>
            <h1><?=$item->title?></h1>
            <div class="variations_form cart">
                <p class="prod-price">$140.00</p>
                <p class="prod-excerpt"><?=$item->content?> <a id="prod-showdesc" class="prod-excerpt-more" href="#">read more</a></p>
                <div class="prod-add">
                    <button type="submit" class="button"><i class="icon ion-android-cart"></i> В корзину</button>
                    <p class="qnt-wrap prod-li-qnt">
                        <a href="#" class="qnt-plus prod-li-plus"><i class="icon ion-arrow-up-b"></i></a>
                        <input type="text" value="1">
                        <a href="#" class="qnt-minus prod-li-minus"><i class="icon ion-arrow-down-b"></i></a>
                    </p>
                </div>
            </div>
            <div class="prod-props">
                <dl class="product_meta">
                    <dt>Артикул:</dt>
                    <dd><?=$item->vendor?></dd>
                    <dt>Тип:</dt>
                    <dd><?=$item->type?></dd>
                    <dt>Открывание:</dt>
                    <dd><?=$item->opening?></dd>
                    <dt>Геометрия:</dt>
                    <dd><?=$item->geometry?></dd>
                    <dt>Стеклопакет:</dt>
                    <dd><?=$item->type_windows?></dd>
                    <dt>Цвет снаружи:</dt>
                    <dd><?=$item->color_outside?></dd>
                    <dt>Цвет внутри:</dt>
                    <dd><?=$item->color_inside?></dd>
                    <dt>Высота окна:</dt>
                    <dd><?=$item->height?></dd>
                    <dt>Ширина окна:</dt>
                    <dd><?=$item->width?></dd>
                    <dt>Параметры:</dt>
                    <dd><?=$item->options?></dd>

                </dl>
            </div>
        </div>
        <p class="prod-badge">
            <span class="badge-1">TOP SELLER</span>
        </p>
    </div>

</article>

