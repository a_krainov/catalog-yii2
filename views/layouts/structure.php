<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!doctype html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <div id="page" class="site">
        <?php $this->beginBody() ?>

        <div class="container-fluid page-styling site-header-before">
            <div class="row">
                <div class="col-lg-4">
                    <ul class="links_list links_list-align-left align-center-desktop topbar-social">
                        <li>
                            <p class="links_list-value">
                                <a href="http://facebook.com" target="_blank" rel="nofollow">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </p>
                        </li>
                        <li>
                            <p class="links_list-value">
                                <a href="mailto:email@email.com" target="_blank" rel="nofollow">
                                    <i class="fa fa-paper-plane"></i>
                                </a>
                            </p>
                        </li>

                        <li>
                            <p class="links_list-value">
                                <a href="http://instagram.com" target="_blank" rel="nofollow">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-8">
                    <ul class="links_list links_list-align-right align-center-desktop topbar-contacts">
                        <li>
                            <p class="links_list-label">
                                Адрес
                            </p>
                            <p class="links_list-value">
                                <a href="http://maps.google.com" target="_blank" rel="nofollow">Москва, ул. Ленина, д.
                                    10</a>
                            </p>
                        </li>
                        <li>
                            <p class="links_list-label">
                                Контакты
                            </p>
                            <p class="links_list-value">
                                <a href="mailto:support@email.com">Support@Email.com</a>
                            </p>
                        </li>
                        <li>
                            <p class="links_list-label">
                                Телефон
                            </p>
                            <p class="links_list-value">
                                <a href="tel:4785929899">(478)-592-9899</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="site-header">
            <p class="h-logo">
                <a href="/"><img src="/img/logo.png" alt="MultiShop"></a>
            </p>
            <div class="h-shop">

                <form method="get" action="#" class="h-search" id="h-search">
                    <input type="text" placeholder="Поиск...">
                    <button type="submit"><i class="ion-search"></i></button>
                </form>

                <ul class="h-shop-links">
                    <li class="h-search-btn" id="h-search-btn"><i class="ion-search"></i></li>


                    <li class="h-cart">
                        <a class="cart-contents" href="cart.html">
                            <p class="h-cart-icon">
                                <i class="ion-android-cart"></i>
                                <span>3</span>
                            </p>
                            <p class="h-cart-total">$510.00</p>
                        </a>
                        <div class="widget_shopping_cart">
                            <div class="widget_shopping_cart_content">
                                <ul class="cart_list">
                                    <li>
                                        <a href="#" class="remove">&times;</a>
                                        <a href="#">
                                            <img src="http://placehold.it/100x67" alt="">
                                            Pneumatic Coil Hose
                                        </a>
                                        <span class="quantity">1 &times; $180.00</span>
                                    </li>
                                    <li>
                                        <a href="#" class="remove">&times;</a>
                                        <a href="#">
                                            <img src="http://placehold.it/100x89" alt="">
                                            Drill Tool Kit
                                        </a>
                                        <span class="quantity">1 &times; $115.00</span>
                                    </li>
                                    <li>
                                        <a href="#" class="remove">&times;</a>
                                        <a href="#">
                                            <img src="http://placehold.it/100x89" alt="">
                                            Searchlight Portable
                                        </a>
                                        <span class="quantity">1 &times; $150.00</span>
                                    </li>
                                </ul>
                                <p class="total"><b>Subtotal:</b> $510.00</p>
                                <p class="buttons">
                                    <a href="cart.html" class="button">View cart</a>
                                    <a href="checkout.html" class="button">Checkout</a>
                                </p>
                            </div>
                        </div>
                    </li>

                    <li class="h-menu-btn" id="h-menu-btn">
                        <i class="ion-navicon"></i> Menu
                    </li>
                </ul>
            </div>
            <div class="mainmenu">
                <nav id="h-menu" class="h-menu">
                    <ul>
                        <li class="active">
                            <a href="/">Главная</a>
                        </li>
                        <li class="">
                            <a href="/">Корзина</a>
                        </li>
                        <li class="">
                            <a href="/">Отдать окно на комиссию</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div id="content" class="site-content">
            <div id="primary" class="content-area width-full">
                <main id="main" class="site-main">
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </main><!-- #main -->
            </div><!-- #primary -->    </div><!-- #content -->
        <div class="container-fluid blog-sb-widgets page-styling site-footer">
            <div class="row">
                <div class="col-sm-12 col-md-4 widget align-center-tablet f-logo-wrap">
                    <a href="/" class="f-logo">
                        <img src="img/logo.png" alt="">
                    </a>
                    <p>Интернет магазин окон</p>
                    <button class="btn callback">Контакты</button>
                </div>
            </div>
        </div>

        <div class="form-validate modal-form" id="modal-form">
            <form action="#" method="POST" class="form-validate">
                <h4>Contact Us</h4>
                <input type="text" placeholder="Your name" data-required="text" name="name">
                <input type="text" placeholder="Your phone" data-required="text" name="phone">
                <input type="text" placeholder="Your email" data-required="text" data-required-email="email"
                       name="email">
                <input class="btn1" type="submit" value="Send">
            </form>
        </div>

        <div class="cont maincont quick-view-modal">
            <article>
                <div class="prod">
                    <div class="prod-slider-wrap prod-slider-shown">
                        <div class="flexslider prod-slider" id="prod-slider">
                            <ul class="slides">
                                <li>
                                    <a data-fancybox-group="prod" class="fancy-img"
                                       href="http://placehold.it/1000x1000">
                                        <img src="http://placehold.it/550x550" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a data-fancybox-group="prod" class="fancy-img"
                                       href="http://placehold.it/1000x1000">
                                        <img src="http://placehold.it/550x550" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a data-fancybox-group="prod" class="fancy-img"
                                       href="http://placehold.it/1000x1000">
                                        <img src="http://placehold.it/550x550" alt="">
                                    </a>
                                </li>
                            </ul>
                            <div class="prod-slider-count"><p><span class="count-cur">1</span> / <span
                                        class="count-all">3</span></p>
                                <p class="hover-label prod-slider-zoom"><i
                                        class="icon ion-search"></i><span>Zoom In</span></p></div>
                        </div>
                        <div class="flexslider prod-thumbs" id="prod-thumbs">
                            <ul class="slides">
                                <li>
                                    <img src="http://placehold.it/550x550" alt="">
                                </li>
                                <li>
                                    <img src="http://placehold.it/550x550" alt="">
                                </li>
                                <li>
                                    <img src="http://placehold.it/550x550" alt="">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="prod-cont">
                        <div class="prod-rating-wrap">
                            <p data-rating="4" class="prod-rating">
                                <i class="rating-ico" title="1"></i><i class="rating-ico" title="2"></i><i
                                    class="rating-ico" title="3"></i><i class="rating-ico" title="4"></i><i
                                    class="rating-ico" title="5"></i>
                            </p>
                            <p class="prod-rating-count">7</p>
                        </div>
                        <p class="prod-categs"><a href="#">Lighting</a>, <a href="#">Tools</a></p>
                        <h2>Belt Sanders</h2>
                        <p class="prod-price">$25.00</p>
                        <p class="stock in-stock">7 in stock</p>
                        <p class="prod-excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada
                            fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget...</p>
                        <div class="prod-add">
                            <button type="submit"
                                    class="button"><i class="icon ion-android-cart"></i> Add to cart
                            </button>
                            <p class="qnt-wrap prod-li-qnt">
                                <a href="#" class="qnt-plus prod-li-plus"><i class="icon ion-arrow-up-b"></i></a>
                                <input type="text" value="1">
                                <a href="#" class="qnt-minus prod-li-minus"><i class="icon ion-arrow-down-b"></i></a>
                            </p>
                            <div class="prod-li-favorites">
                                <a href="wishlist.html" class="hover-label add_to_wishlist"><i
                                        class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                            </div>
                            <p class="prod-li-compare">
                                <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i
                                        class="icon ion-arrow-swap"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div><!-- #page -->
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>