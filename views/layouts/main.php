<?

$this->beginContent('@app/views/layouts/structure.php');
use yii\widgets\Breadcrumbs;
?>
    <div class="cont maincont">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'b-crumbs'],
        ]) ?>
        <? echo $content; ?>
    </div>
<?
$this->endContent();