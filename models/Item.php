<?php
/**
 * Created by PhpStorm.
 * User: krainov
 * Date: 05.04.2018
 * Time: 1:50
 */

namespace app\models;
use yii\db\ActiveRecord;

class Item extends ActiveRecord
{
    public static function tableName()
    {
        return 'item';
    }

}