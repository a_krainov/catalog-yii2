<?php
/**
 * Created by PhpStorm.
 * User: krainov
 * Date: 04.04.2018
 * Time: 23:47
 */

namespace app\controllers;

use app\models\Item;
use yii\data\Pagination;
use yii\web\HttpException;

class CatalogController extends AppController
{

    public function actionIndex($name = '')
    {
        $this->layout = 'index';
        //$items = Item::find()->orderBy('id DESC')->all();

        // Пагинация
        $query = Item::find()->orderBy('id DESC');
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3, 'pageSizeParam' => false, 'forcePageParam' => false]);
        $items = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', compact('items', 'pages'));
    }

    public function actionView(){
        $id = \Yii::$app->request->get('id');
        $item = Item::findOne($id);
        if(empty($item)) throw new HttpException(404, 'Такой страницы не существует');

        return $this->render('view', compact('item'));
    }
}