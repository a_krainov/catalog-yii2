<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Montserrat:400,700,900%7COpen+Sans:300,300i,400,400i,600,600i,700,700i&amp;subset=cyrillic',
        'css/font-awesome.min.css',
        'css/ionicons.min.css',
        'css/bootstrap.css',
        'css/jquery.formstyler.css',
        'css/flexslider.css',
        'css/jquery.fancybox.css',
        'css/ion.rangeSlider.css',
        'css/jquery.mThumbnailScroller.css',
        'css/chosen.css',
        'css/style.css',
        'css/elements.css',
        'css/media.css',
        'css/elements-media.css',
        'css/site.css',
    ];
    public $js = [
        //'js/jquery-1.12.4.min.js',
        'js/jquery-plugins.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
